SOURCES += \
    8.31/pcre_byte_order.c \
    8.31/pcre_chartables.c \
    8.31/pcre_compile.c \
    8.31/pcre_config.c \
    8.31/pcre_dfa_exec.c \
    8.31/pcre_exec.c \
    8.31/pcre_fullinfo.c \
    8.31/pcre_get.c \
    8.31/pcre_globals.c \
    8.31/pcre_jit_compile.c \
    8.31/pcre_maketables.c \
    8.31/pcre_newline.c \
    8.31/pcre_ord2utf8.c \
    8.31/pcre_printint.c \
    8.31/pcre_refcount.c \
    8.31/pcre_string_utils.c \
    8.31/pcre_study.c \
    8.31/pcre_tables.c \
    8.31/pcre_ucd.c \
    8.31/pcre_valid_utf8.c \
    8.31/pcre_version.c \
    8.31/pcre_xclass.c \
    8.31/pcrecpp.cpp
